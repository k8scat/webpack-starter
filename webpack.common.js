const path = require("path");
const CleanWebpackPlugin = require("clean-webpack-plugin");
const HtmlWebpackPlugin = require("html-webpack-plugin");

module.exports = {
  entry: {
    index: path.resolve(__dirname, "./src/js/index.js")
  },
  output: {
    filename: "js/[name].js",
    path: path.resolve(__dirname, "dist")
  },
  plugins: [
    new CleanWebpackPlugin(["dist"]),
    new HtmlWebpackPlugin({
      title: "webpack-starter",
      template: path.resolve(__dirname, "./src/index.html")
    })
  ],
  module: {
    rules: [
      // load images
      {
        test: /\.(ico|jpg|jpeg|png|gif|webp|svg)$/,
        use: {
          loader: "file-loader",
          options: {
            name: "images/[name].[ext]"
          }
        }
      },
      // load fonts
      {
        test: /\.(woff|woff2|eot|ttf|otf)$/,
        use: {
          loader: "file-loader",
          options: {
            name: "fonts/[name].[ext]"
          }
        }
      },
      // load music
      {
        test: /\.(mp3|wav|ogg)$/,
        use: {
          loader: "file-loader",
          options: {
            name: "musics/[name].[ext]"
          }
        }
      }
    ]
  },

};