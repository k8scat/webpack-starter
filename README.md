# webpack-starter

Quick start with webpack!

## Use
```bash
$ git clone git@github.com:hsowan/webpack-starter.git
$ cd webpack-starter
$ npm install
$ npm start
```

## Support Load
+ [x] images(file-loader)
+ [x] musics(file-loader)
+ [x] json(build-in)
+ [x] css/sass(style-loader, css-loader, sass-loader)



## Baidu SEO
```bash
$ npm i baidu-autopush
```

## Reference
+ [Official Webpack Document](https://webpack.js.org/concepts/)
+ [Webpack Document in Chinese](https://www.webpackjs.com/concepts/)